# clap-privacy

List of association/organization that advocate about privacy (Not tools) (99 so far) : 


To do privacy : 

https://www.gp-digital.org/

https://www.privacy-ticker.com/




World : 
        
        EFF : https://www.eff.org/
        Privacy International : https://www.privacyinternational.org/
        Article 19 : https://www.article19.org/
        epic.org : https://epic.org/
        World privacy forum : https://www.worldprivacyforum.org/
        Future of privacy forum : https://fpf.org/
        Global Encryption : https://www.globalencryption.org/
        Privacy Interest Group (PING) : https://www.w3.org/Privacy/IG/
        ISRG : https://www.abetterinternet.org/
        Global Network Initiative : https://globalnetworkinitiative.org
        Privacy Enforcement (not very active ?) : https://www.privacyenforcement.net
        P=P (International ?): https://pep.foundation
        Internet Society Fondation : https://www.internetsociety.org
    
Europe : 
            
        accessnow : https://www.accessnow.org
        fsfe : https://fsfe.org/
        noyb (none of your business) : https://noyb.eu/
        liberties : https://www.liberties.eu
        guardint : https://guardint.org/
        https://www.patrick-breyer.de 
        https://publicspaces.ne
        https://gdprhub.eu
        

Argentina : 

        https://adc.org.ar/ https://adc.org.ar/en/home/ 


Austria : 
    
        Epicenter.works (also in en)
        vibe!at https://www.vibe.at/ Translated : https://www.translatetheweb.com/?ref=TVert&from=&to=en&a=https%3A%2F%2Fwww.vibe.at%2F
        quintessenz : http://www.quintessenz.org/ Translate : http://www.translatetheweb.com/?from=&to=en&ref=TVert&refd=www.bing.com&dl=en&rr=DC&a=http%3a%2f%2fwww.quintessenz.at%2f
        Privacy Week : https://privacyweek.at
    
Azerbaijan : 

        Azerbaijan Internet Watch https://www.az-netwatch.org/
    
Belgium :

        Ministry Of Privacy : https://ministryofprivacy.eu/ ( Not english )
        Technopolice for belgium : https://technopolice.be/
        Data panik : https://www.datapanik.org/

Bolivia : 

        Internet Bolovia : 
            ES -> https://internetbolivia.org/
            Translated ES -> EN : https://www.translatetheweb.com/?ref=TVert&from=&to=en&a=https%3A%2F%2Finternetbolivia.org%2F#
Brazil :    

        https://www.codingrights.org/ 
        https://dataprivacy.com.br/

Bulgaria :

        Internet Society Bulgaria :
            BG -> https://isocbg.wordpress.com/
            Translated ES -> EN : https://www.translatetheweb.com/?from=bg&to=en&dl=en&ref=trb&a=https%3A%2F%2Fisocbg.wordpress.com%2F
    
Canada : 

        Open Privacy : https://openprivacy.ca/
        Citizen lab : https://citizenlab.ca/
         
Colombia : 

        Fundación Karisma : https://web.karisma.org.co/ 
        
Croatia : 

        gong : 
            CR -> https://www.gong.hr/hr/  
            EN -> https://www.gong.hr/en/  
            Translated CR -> EN : https://www.translatetheweb.com/?ref=TVert&from=&to=en&a=https%3A%2F%2Fwww.gong.hr%2Fhr%2F
            
Czech Republic : 

Central America : 

        https://www.ipandetec.org/
    
Denmark :

        https://itpol.dk/

Egypt : 

        Masaar : https://masaar.net/ ( Arabic )

Estonia : 

Finland : 

        effi :  https://effi.org/ Translated : https://www.translatetheweb.com/?ref=TVert&from=&to=en&a=https%3A%2F%2Feffi.org%2F
    
France : 

        La Quadrature du Net : 
            FR -> https://www.laquadrature.net/ 
            EN -> https://www.laquadrature.net/en ( Less article in EN ) 
            ES -> https://laquadrature.net/es ( Even less article in ES ) 
            Translated fr -> en https://www.translatetheweb.com/?from=fr&to=en&dl=en&ref=trb&a=https%3A%2F%2Fwww.laquadrature.net%2F%3Flang%3Dfr
            
        Technopolice for France : https://technopolice.fr/
        
        EDNEthique : https://adnethique.org/
        
        etraces : https://etraces.constantvzw.org/informations/index.php?lang=fr https://etraces.constantvzw.org/informations/index.php?lang=en

        
        Stop loi securite globale : https://stoploisecuriteglobale.fr/

        
Germany : 

        netzpolitik (about freedomrights)
            DE ->  https://netzpolitik.org/
            Translated DE -> EN https://www.translatetheweb.com/?ref=TVert&from=&to=en&a=https%3A%2F%2Fdigitalegesellschaft.de%2F
            
            
        Digital Courage (Also about UE, and in english): 
            DE -> https://digitalcourage.de
            EM -> https://digitalcourage.de/en
            Translated DE -> EN https://www.translatetheweb.com/?ref=TVert&from=&to=en&a=https%3A%2F%2Fdigitalcourage.de
            
        Digitale Gesellschaft : 
            DE : https://digitalegesellschaft.de/
            Translated DE -> EN  https://www.translatetheweb.com/?ref=TVert&from=&to=en&a=https%3A%2F%2Fdigitalegesellschaft.de%2F
            
        Chaor Computer Club (Also in EN): https://www.ccc.de/en/topics 
        
Ghana

        African Cybersecurity and Digital Rights Organisation (ACDRO) : http://acdro.org/  
        
        
Greece : 

        homo digitalis : 
            GR -> https://www.homodigitalis.gr 
            EN ->  https://www.homodigitalis.gr/en
            GR -> EN https://www.translatetheweb.com/?ref=TVert&from=&to=en&a=https%3A%2F%2Fwww.homodigitalis.gr%2F
            
Kenya : 
    
    
         KICTANet : https://www.kictanet.or.ke/
    
    
Hungary : 
    
    

India : 

        IFF : https://internetfreedom.in/
        https://sflc.in/
        
Indonesia : 

        ICT Watch : https://ictwatch.id/
    
Irland : 

        Digital right Ireland : https://www.digitalrights.ie/
    
Italy : 
    
        ALCEI : 
            IT -> https://www.alcei.it/ 
            Translated IT -> EN https://www.translatetheweb.com/?ref=TVert&from=&to=en&a=https%3A%2F%2Fwww.alcei.it%2F
            
        CILD : 
            IT -> https://cild.eu/ 
            EN -> https://cild.eu/en/
            Translated IT -> EN 
            
        Hermes Center : https://www.hermescenter.org/
        
        https://www.privacy-network.it/ 

Jordan :

    
        https://jordanopensource.org/ 
    
    
Lebanese :
    
        https://smex.org/ 


Nigeria :
    
        https://paradigmhq.org/  
    
Norway : 
    
        EFN : (NO) https://efn.no/
        
        forbrukerradet : 
            EM ->https://www.forbrukerradet.no/    
            NO -> EN https://www.translatetheweb.com/?ref=TVert&from=&to=en&a=https%3A%2F%2Fwww.forbrukerradet.no%2F#
            

The Netherlands 

        https://www.vrijschrift.org/ 

    
Malaysia : 

    IO Foundation : https://www.theiofoundation.org
    Sinar Project : https://sinarproject.org/

Mexico : 

        r3d : 
            ES -> www.r3d.mx
            ES -> EN  https://www.translatetheweb.com/?from=&to=en&dl=en&ref=trb&a=https%3A%2F%2Fr3d.mx%2F
            
        http://www.usuariosdigitales.org
        
        https://seguridad-digital.org/

    
Poland : 
    

        Centrum cyfrowe ( Also in EN) : https://centrumcyfrowe.pl/en/homepage/
        Panoptykon    (Also in EN) : https://en.panoptykon.org/

Pakistan : 

        https://digitalrightsfoundation.pk/ 
    
Paraguay : 
    
        https://www.tedic.org/ https://www.tedic.org/en/ 
        
Peru  :

        https://hiperderecho.org/ 

Porto rico 

        https://acceso.or.cr/index.html 


Portugal : 

        D3 Defesa dos Direitos Digitais :
            PT -> https://direitosdigitais.pt/   
            PT -> EN https://www.translatetheweb.com/?ref=TVert&from=&to=en&a=https%3A%2F%2Fdireitosdigitais.pt%2F
            
        Privacy|lx : 
            PT -> https://privacylx.org 
            EN -> https://privacylx.org/en/   
            PT -> EN 3https://www.translatetheweb.com/?ref=TVert&from=&to=en&a=https%3A%2F%2Fprivacylx.org%2F
Russia : 

        https://roskomsvoboda.org/ 


Senegal :
    
        https://asutic.org/en/home/   https://asutic.org/fr/accueil/

        https://blog.asutic.org/  

Spain : 

        Xnet : https://xnet-x.net/
        
        Pangea : https://pangea.org/
        
        Nodo 50 : 
            ES -> https://info.nodo50.org/
            ES -> EN https://www.translatetheweb.com/?ref=TVert&from=&to=en&a=https%3A%2F%2Finfo.nodo50.org%2F

South Korea : 

    http://opennetkorea.org/en/wp/


Sweden : 

        dataskydd : https://dataskydd.net/
        DFRI : https://www.dfri.se/
        Nordic Privacy : https://nordicprivacy.org/

Turkey : 

    
    
UK : 

        Big Brother Watch : https://bigbrotherwatch.org.uk/
        defenddigitalme : https://defenddigitalme.org/news/
        Open Rights Group : https://www.openrightsgroup.org/

USA : 

        Privacy rights -> https://privacyrights.org/
        Center for democracy and technology : https://cdt.org/
        Demand progress  : https://demandprogress.org/
        Fight for the future : https://www.fightforthefuture.org/
        https://www.stopspying.org/ 
        Battle for the net : https://www.battleforthenet.com/#nav
        Open MIC : https://www.openmic.org/
        Ranking Digital Rights : https://rankingdigitalrights.org
        https://iapp.org/
        
Venezuela : 


        https://ipysvenezuela.org/ 

        https://veinteligente.org/
Zimbabwe : 

        https://www.digitalsociety.africa

----

To do open source : https://softwarefreedom.org/ https://ocf.tw/ https://ocf.tw/en/
https://www.oseno.nl/
https://shared-digital.eu/

https://fosshost.org/
https://interhop.org/

To do other : https://freedomonlinecoalition.com/
https://www.isocfoundation.org/

https://www.publicknowledge.org/ (Do not work with tor )
https://offcourse.io/about
https://www.oseno.nl/
https://publicopen.space
https://sdeps.eu
News : https://waag.org/en

List of association/organization that advocate for Open source (Not tools) (7 so far) : 

Albania : 

        Open labs : https://openlabs.cc/en/

Which country ? 

        https://techcultivation.org/
        https://equalit.ie/

Europe : 

        Next Internet Generation : https://www.ngi.eu/ngi-projects/ngi-zero/
    
        Public code : https://publiccode.eu/
    
France : 

        https://aful.org/ 
        
        Framasoft  (also in en): https://framasoft.org/


Germany : 


        Open UK : https://openuk.uk/
    

Netherland : 

        nlnet.nl (More open source focused that privacy): https://nlnet.nl/
        Bits Of Freedom (Also in english ): https://www.bitsoffreedom.nl/english/
    

----



List of association/organization that advocate for Security (Not tools) (2 so far) : 

Netherland : 

        https://internet.nl/



List of association/organization that advocate against copyright (Not tools) : 
    
        https://www.defectivebydesign.org/
        
----

List of subreddit that advocate about privacy (Not tools) () : 


/r/europrivacy
/r/privacy (US)
/r/netpolitics
/r/privacyworld 

----

Matrix room : #time-to-list:matrix.org

Why this list ? 
    
 
    Because I never found a complete one on internet 
    

What you will do about this list ? 
    
    I will put it on lemmy/reddit and after create a github or codeberg for it

    This is the first step of many 
    The next one is to build an interactive website site about those list
    Then following up with aggregating the rss of all of them to make it easier to access news from one place about privacy/opensource/security
   And a lot more
    
    
This list is licensed under a Creative Commons BY-SA 4.0 Licence.